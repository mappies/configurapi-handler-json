const assert = require('chai').assert;
const sinon = require('sinon');
const Configurapi = require('configurapi');

const PageResponse = require('../src/pageResponse');

describe('PageResponse', function() {
    it("Serialize object properly", async function()
    {    
        let response = new PageResponse(['b', 'c', 'd'],'n', 'p', {extra:false}, 202, {'X-Header': 'value'});
        
        assert.equal('n', response.body.nextPageToken);
        assert.equal('p', response.body.prevPageToken); 
        assert.equal(false, response.body.extra);
        assert.equal('b', response.body.items[0]);
        assert.equal('c', response.body.items[1]);
        assert.equal('d', response.body.items[2]);
        assert.equal(202, response.statusCode);
        assert.equal('application/json', response.headers['Content-Type']);
        assert.equal('value', response.headers['X-Header']);
    });

    it("Reject non-array", async function()
    {    
        let error = undefined;

        try
        {
            new PageResponse("hi");
        }
        catch(e)
        {
            error = e;
        }

        assert.isDefined(error);
        assert.equal(error.statusCode, 500);
        assert.equal(error.message, 'An array is expcted for PageResponse.');
    });

    it("Accept undefined", async function()
    {    
        let response = new PageResponse(undefined);

        assert.isUndefined(response.body.nextPageToken);
        assert.isUndefined(response.body.prevPageToken);
    });
});