const assert = require('chai').assert;
const sinon = require('sinon');
const Configurapi = require('configurapi');

const validateJsonRequestHandler = require('../src/validateJsonRequestHandler');

describe('validateJsonRequestHandler', function() {
    it("Validate a valid empty payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = undefined;

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayload']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid null empty payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = null;

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayload']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate an invalid empty payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {};

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayload']);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid optional payload - with a valid payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:'aName'};

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayloadAccepted']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid optional payload - with an invalid payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload =  {name:3.14};

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayloadAccepted']);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid optional payload - without payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayloadAccepted']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid optional payload - with null payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = null;

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayloadAccepted']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });


    it("Validate a valid payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:'aName'};

        await validateJsonRequestHandler.apply(context, [ev]);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate an invalid payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:3.14};

        await validateJsonRequestHandler.apply(context, [ev]);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
        assert.equal('data/name must be string', ev.response.message);
        assert.equal(1, ev.response.details.length);
    });

    it("Validate a valid payload with a custom schema file", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'something_note_post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:'aName'};

        await validateJsonRequestHandler.apply(context, [ev, 'post_object']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Schema file not found", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'schema_not_exist_event';
        ev.response = new Configurapi.Response();

        await validateJsonRequestHandler.apply(context, [ev]);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid payload when format specified in schema", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:'aName', date: '2000-01-01'};

        await validateJsonRequestHandler.apply(context, [ev]);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });
    
    it("Validate an invalid payload when format specified in schema", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:'aName', date:'not-a-date'};

        await validateJsonRequestHandler.apply(context, [ev]);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
        assert.equal('data/date must match format "date"', ev.response.message);
        assert.equal(1, ev.response.details.length);
    });
});