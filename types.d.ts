import { IEvent, Response } from "configurapi";

declare class JsonResponse extends Response
{
    jsonReplacer?: any;
    constructor(obj: object, statusCode?: number, headers?: Record<string, string>, jsonReplacer?: any);
}

declare class ListResponse extends JsonResponse
{
    constructor(collection: any[], additionalProperties?: object, statusCode?: number, headers?: Record<string, string>);
}

declare class PageResponse extends JsonResponse
{
    constructor(collection: any[], nextPageToken?: string, prevPageToken?: string, additionalProperties?: object, statusCode?: number, headers?: Record<string, string>)
}

declare function setJsonResponseHandler(event: IEvent, statusCode?: number, body?: Record<string, any> | string | undefined, headers?: Record<string, string>);

declare function validateJsonRequestHandler(event: IEvent, mode: string);