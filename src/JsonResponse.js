const Response = require('configurapi').Response;

module.exports = class JsonResponse extends Response
{
    constructor(obj, statusCode = 200, headers = {}, jsonReplacer = undefined)
    {
        super(obj, statusCode, headers);

        if(jsonReplacer != undefined)
        {
            this.jsonReplacer = jsonReplacer;
        }
        else if('jsonReplacer' in obj)
        {
            this.jsonReplacer = obj.jsonReplacer;
        }
        else
        {
            this.jsonReplacer = undefined;
        }
        
        if(!('Content-Type' in this.headers))
        {
            this.headers['Content-Type'] = 'application/json';
        }
    }
};