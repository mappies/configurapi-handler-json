const JsonResponse = require('./JsonResponse');
const ErrorResponse = require('configurapi').ErrorResponse;

module.exports = class PageResponse extends JsonResponse
{
    constructor(collection, nextPageToken = undefined, prevPageToken = undefined, additionalProperties = {}, statusCode = 200, headers = {})
    {
        if(collection && collection.constructor !== Array)
        {
            throw new ErrorResponse("An array is expcted for PageResponse.", 500);
        }

        super(Object.assign({nextPageToken: nextPageToken, prevPageToken: prevPageToken, items: collection}, additionalProperties), statusCode, headers);
    }
};